# CHANGELOG

This project adheres to [Semantic Versioning](http://semver.org/).
Which is based on [Keep A Changelog](http://keepachangelog.com/)

## [Unreleased]

### Added

- add support alpn in bind option
- add error files
- use multiple ssl certificates on one frontend
- test: add support debian 12

### Changed

- test: use personal docker registry

### Removed

- test: remove support debian 10

## v1.1.0 - 2021-08-15

### Added

- add userlist
- can specify the haproxy repository
- add resolvers configuration
- add debian11 support
- add IPs lists

### Changed

- change default group for stat socket
- reload haproxy after certificate change
- chore: use FQCN for module name
- test: replace kitchen to molecule

## v1.0.0 - 2019-11-09

### Added

- first version
