# Ansible role: Haproxy

[![Version](https://img.shields.io/badge/latest_version-1.1.0-green.svg)](https://code.waks.be/nishiki/ansible-role-haproxy/releases)
[![License](https://img.shields.io/badge/license-Apache--2.0-blue.svg)](https://code.waks.be/nishiki/ansible-role-haproxy/src/branch/main/LICENSE)
[![Build](https://code.waks.be/nishiki/ansible-role-haproxy/actions/workflows/molecule.yml/badge.svg?branch=main)](https://code.waks.be/nishiki/ansible-role-haproxy/actions?workflow=molecule.yml)

Install and configure haproxy

## Requirements

- Ansible >= 2.10
- Debian
  - Bullseye
  - Bookworm

## Role variables

- `haproxy_apt_release` - set the apt release to use (default: `ansible_distribution_release`)
- `haproxy_global` - hash with the global configuration

```
  log-send-hostname: true
  chroot: /var/lib/haproxy
  pidfile: /var/run/haproxy.pid
  maxconn: 4096
  user: haproxy
  group: haproxy
  daemon: true
  stats: socket /var/lib/haproxy/stats
```

- `haproxy_defaults` - hash with the defaults configurations

```
  mode: http
  log: global
  option:
    - httplog
    - dontlognull
    - http-server-close
    - forwardfor except 127.0.0.0/8
    - redispatch
  retries: 3
  timeout:
    - http-request 10s
    - queue 1m
    - connect 10s
    - client 1m
    - server 1m
    - http-keep-alive 10s
    - check 10s
  maxconn: 4096
```

- `haproxy_stats_username` - set the username to access stats interface (default: `admin`)
- `haproxy_stats_password`- set the password to access stats interface (default: `secret`)
- `haproxy_listen_stats` - hash with options for stats interface

```
  bind:
    ip: 127.0.0.1
    port: 9000
  mode: http
  maxconn: 10
  timeout:
    - client 30s
    - server 30s
    - connect 30s
    - queue 30s
  stats:
    - 'refresh 5s'
    - 'show-node'
    - 'realm Haproxy\ Statistics'
    - 'auth {{ haproxy_stats_username }}:{{ haproxy_stats_password }}'
    - 'uri /haproxy_stats'
```

- `haproxy_resolvers` - hash with the resolvers configuration

```
  dns:
    local:
      - dns1 127.0.0.1:53
```

- `haproxy_frontends` - hash with the frontends configuration

```
  fontend-https:
    bind:
      ip: '*'
      port: 443
      ssl:
        cert:
          - /etc/haproxy/ssl1.pem
          - /etc/haproxy/ssl2.pem
    default_backend: backend-app
  frontend-http:
    bind:
      ip: '*'
      port: 80
    mode: http
    option:
      - 'forwardfor except 127.0.0.0/8'
      - 'httplog'
      - 'http-server-close'
    acl:
      - acl_test1-https hdr(host) test1.example.net
      - acl_test2-https hdr(host) test2.example.net
      - acl_blacklist src -f /etc/haproxy/blacklist.list
    use_backend:
      - backend-app if acl_test1-https
      - backend-app if acl_test2-https
      - backend-app if acl_test1-https acl_blacklist
```

- `haproxy_backends` - hash with the backend configuration

```
  backend-app:
    mode: http
    balance: roundrobin
    stick-table: type ip size 512k expire 30m
    stick: on src
    option:
      - 'ssl-hello-chk'
      - 'tcp-check'
      - 'log-health-checks'
      - 'forwardfor header X-Forwarded-For'
    server:
      - host1 127.0.0.1:80 check maxconn 1000 inter 15s
      - host2 127.0.0.1:80 check maxconn 1000 inter 15s
```

- `haproxy_ips_lists` - hash with the ips list to use in haproxy

```
  blacklist:
    - 192.168.0.0/16
    - 172.10.10.0/24
  public_withlist:
    - 8.8.8.8
```

- `haproxy_userlists` - hash with the userlist for basic auth

```
  restricted:
    group:
      - admin
    user:
      - admin password 84375611a53741f7e94b09eb49127f41 groups admin
```

- `haproxy_ssl_certificates` - hash with ssl certificates to copy

```
  example-com: |
		-----BEGIN CERTIFICATE-----
		MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQDJZs7pOmgIjXqa
		/rZx2CL4pXTo4kKv+Itj3xfrq46r4DZKQJoCHqcs/bFjtCP89xJlvbrZVmxizFOF
		mfKiNat39GNgeUaTH23+N/FOAyphayOcFk+s4X6GbdxrkHqt9IBrfK0B88bYjL/m
		.....
```

- `haproxy_http_errors` - hash with http error groups

```
  test:
    503: test-503
```

- `haproxy_http_errors_file` - hash with http error files

```
  test-503: |
    HTTP/1.1 503 Service Unavailable
    Cache-Control: no-cache
    Connection: close
    Content-Type: text/html

    <!DOCTYPE html>
    <html lang="en">
        <body>
            <main>
                This is my custom 503 page
            </main>
        </body>
    </html>
```

## How to use

```
- hosts: server
  roles:
    - haproxy
```

## Development

### Test with molecule and docker

- install [docker](https://docs.docker.com/engine/installation/)
- install `python3` and `python3-pip`
- install molecule and dependencies `pip3 install molecule 'molecule[docker]' docker ansible-lint testinfra yamllint`
- run `molecule test`

## License

```
Copyright (c) 2019 Adrien Waksberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
